const express = require('express');
const router = express.Router();
const auth = require('../security/auth');
const { verifyToken } = auth;

const productControllers = require('../controllers/productControllers');
router.post('/add', productControllers.addProduct);

router.get('/item/:id', verifyToken, productControllers.getSingleProduct);

router.get('/categories/:category', productControllers.getProductCategories);

module.exports = router;
