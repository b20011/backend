const express = require('express');
const router = express.Router();
const auth = require('../security/auth');
const { verifyToken } = auth;

const bookingController = require('../controllers/bookingControllers');
//Book appointment
router.post('/new', bookingController.bookAppointment);

//Retrieve all appointment
router.get('/all', bookingController.getAllAppointments);

router.post('/mail', bookingController.sendMail);

//Delete an appointment
router.delete('/:id', bookingController.deleteAppointment);

module.exports = router;
