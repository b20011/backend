const express = require('express');
const router = express.Router();

const quotationControllers = require('../controllers/quotationController');

router.post('/request', quotationControllers.requestQuotation);

router.get('/all', quotationControllers.getAllQuotes);

module.exports = router;
