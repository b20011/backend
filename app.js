const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const connectDB = require('./config/db');

const app = express();
const PORT = process.env.PORT || 4000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

connectDB();

const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);

const productRoutes = require('./routes/productRoutes');
app.use('/products', productRoutes);

const adminRoutes = require('./routes/adminRoutes');
app.use('/admin', adminRoutes);

const bookingRoutes = require('./routes/bookingRoutes');
app.use('/booking', bookingRoutes);

const quotationRoutes = require('./routes/quotationRoutes');
app.use('/quotes', quotationRoutes);

app.listen(PORT, () => console.log(`Listening in port : ${PORT}`));
