const jwt = require('jsonwebtoken');
const config = require('config');

const generateToken = (user) => {
  const payload = {
    user: {
      id: user._id,
      email: user.email,
    },
  };
  return jwt.sign(payload, config.get('jwtSecret'), { expiresIn: '1hr' });
};

const verifyToken = (req, res, next) => {
  const token = req.headers.authorization;

  if (!token) {
    return res.status(401).json({ msg: 'No token, authorization failed.' });
  }

  try {
    const slicedToken = token.split(' ')[1];
    jwt.verify(slicedToken, config.get('jwtSecret'), (err, decodedToken) => {
      if (err) res.send({ auth: 'Failed', message: err.message });

      req.user = decodedToken;

      next();
    });
  } catch (err) {
    res.status(401).json({ msg: 'Token is not valid' });
  }
};

module.exports = {
  generateToken,
  verifyToken,
};
