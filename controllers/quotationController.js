const Quotation = require('../models/Quotation');
const { capitalize } = require('../controllers/bookingControllers');

const requestQuotation = async (req, res) => {
  try {
    const {
      firstName,
      lastName,
      phoneNumber,
      email,
      budget,
      nameOfCelebration,
      eventStart,
      eventEnd,
      colourTheme,
      eventAddress,
      itemsRequired,
      deliveryTime,
      eventType,
      balloonType,
      additionalComments,
      productsInquiry,
      quantity,
    } = req.body;

    const first = capitalize(firstName);
    const second = capitalize(lastName);

    const request = new Quotation({
      firstName: first,
      lastName: second,
      phoneNumber,
      email,
      budget,
      nameOfCelebration,
      eventStart,
      eventEnd,
      colourTheme,
      eventAddress,
      itemsRequired,
      deliveryTime,
      eventType,
      balloonType,
      additionalComments,
      productsInquiry,
    });

    const query = await request.save();

    res.send(query);
  } catch (error) {
    res.send(error);
  }
};

const getAllQuotes = async (req, res) => {
  try {
    const quote = await Quotation.find({});
    res.send(quote);
  } catch (error) {
    res.send(error);
  }
};

module.exports = {
  requestQuotation,
  getAllQuotes
};
