const bcrypt = require('bcryptjs');

const Admin = require('../models/Admin');
const auth = require('../security/auth');

const registerAdmin = (req, res) => {
  const { firstName, lastName, email, password, mobileNo } = req.body;

  Admin.countDocuments({ email }, (err, count) => {
    if (err) res.send(err);

    const salt = bcrypt.genSaltSync(10);
    const hashedPassword = bcrypt.hashSync(password, salt);

    if (count === 0) {
      const newAdmin = new Admin({
        firstName,
        lastName,
        email,
        password: hashedPassword,
        mobileNo,
      });

      newAdmin
        .save()
        .then((data) => res.send(data))
        .catch((err) => res.send(err));
    } else {
      res.send(false);
    }
  });
};

const adminLogin = (req, res) => {
  const { email, password } = req.body;
  Admin.countDocuments({ email }, (err, count) => {
    if (err) res.send(err);

    if (count !== 0) {
      Admin.findOne({ email }).then((data) => {
        const unhashedPassword = bcrypt.compareSync(password, data.password);

        if (unhashedPassword) {
          res.send({ token: auth.generateToken(data) });
        } else {
          // res.send(false);
          res.send({ error: 'Incorrect password', value: false });
        }
      });
    } else {
      // res.send(false);
      res.send({ error: 'Invalid email', value: false });
    }
  });
};

module.exports = {
  registerAdmin,
  adminLogin,
};
