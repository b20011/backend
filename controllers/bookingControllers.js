const Booking = require('../models/Booking');
const nodemailer = require('nodemailer');
const config = require('config');

const capitalize = (name) => {
  const first = name.split('')[0].toUpperCase();
  const second = name.slice(1, name.length).toLowerCase();
  return `${first}${second}`;
};

const bookAppointment = async (req, res) => {
  try {
    const {
      firstName,
      lastName,
      phoneNumber,
      email,
      budget,
      nameOfCelebration,
      eventStart,
      eventEnd,
      colourTheme,
      eventAddress,
      itemsRequired,
      deliveryTime,
      eventType,
      balloonType,
      additionalComments,
    } = req.body;

    const first = capitalize(firstName);
    const second = capitalize(lastName);

    const bookEvent = new Booking({
      firstName: first,
      lastName: second,
      phoneNumber,
      email,
      budget,
      nameOfCelebration,
      eventStart,
      eventEnd,
      colourTheme,
      eventAddress,
      itemsRequired,
      deliveryTime,
      eventType,
      balloonType,
      additionalComments,
    });

    const book = await bookEvent.save();

    res.send(book);
  } catch (error) {
    res.send({ message: error, error: true });
  }
};

const getAllAppointments = async (req, res) => {
  try {
    const findAll = await Booking.find({});

    res.send(findAll);
  } catch (error) {
    res.send(error);
  }
};

const deleteAppointment = async (req, res) => {
  try {
    const deleteBooking = Booking.findOneAndDelete(
      { _id: req.params.id },
      (err, docs) => {
        if (err) {
          console.log(err);
        } else {
          console.log('Deleted user', docs);
        }
      }
    );

    res.send(deleteBooking);
  } catch (error) {
    res.send(error);
  }
};

const sendMail = (req, res) => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: config.get('email'),
      pass: config.get('pass'),
    },
  });

  transporter
    .sendMail({
      from: `"Crystal Decor" ${config.get('email')}`, // sender address
      to: req.body.to, // list of receivers
      subject: req.body.subject, // Subject line
      text: req.body.text, // plain text body
    })
    .then((info) => {
      console.log({ info });
      res.send({ info });
    })
    .catch(console.error);
};

module.exports = {
  capitalize,
  bookAppointment,
  getAllAppointments,
  deleteAppointment,
  sendMail,
};
