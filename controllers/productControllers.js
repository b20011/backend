const Products = require('../models/Products');

const addProduct = (req, res) => {
  try {
    const { name, description, image, category } = req.body;

    Products.countDocuments({ name }, async (err, count) => {
      if (err) res.send(err);

      if (count === 0) {
        const newProduct = new Products({
          name,
          description,
          image,
          category,
        });

        let product = await newProduct.save();
        res.send(product);
      } else {
        res.send({ message: 'Product name already exists', error: true });
      }
    });
  } catch (error) {
    res.send(error);
  }
};

const getSingleProduct = async (req, res) => {
  try {
    const product = await Products.findById(req.params.id);

    res.send(product);
  } catch (error) {
    res.send(error);
  }
};

const getProductCategories = async (req, res) => {
  try {
    const productCategory = await Products.find({
      category: req.params.category,
    });

    res.send(productCategory);
  } catch (error) {
    res.send({
      message: "There's an error in getting the category",
      error: true,
    });
  }
};

//Delete products added by the admin
const deleteSingleProduct = (req, res) => {
  try {
    Products.findByIdAndDelete(req.body.id);
  } catch (error) {
    res.send(error);
  }
};

module.exports = {
  addProduct,
  getSingleProduct,
  getProductCategories,
  deleteSingleProduct,
};
