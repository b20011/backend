const mongoose = require('mongoose');

const bookingSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, 'First name cannot be empty'],
  },
  lastName: {
    type: String,
    required: [true, 'Last name cannot be empty'],
  },
  phoneNumber: {
    type: String,
    required: [true, 'Phone number cannot be empty'],
  },
  email: {
    type: String,
    required: [true, 'Email cannot be empty'],
  },
  budget: {
    type: String,
  },
  nameOfCelebration: {
    type: String,
  },
  eventStart: {
    type: String,
    required: [true, 'Start of event cannot be empty'],
  },
  eventEnd: {
    type: String,
    required: [true, 'Start of event cannot be empty'],
  },
  colourTheme: {
    type: String,
  },
  eventAddress: {
    type: String,
  },
  itemsRequired: {
    type: String,
  },
  deliveryTime: {
    type: String,
  },
  eventType: {
    type: String,
    required: [true, 'Event type required cannot be empty'],
  },
  balloonType: {
    type: String,
  },
  additionalComments: {
    type: String,
  },
});

module.exports = mongoose.model('Booking', bookingSchema);
