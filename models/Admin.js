const mongoose = require('mongoose');

const adminSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, 'First name cannot be empty'],
  },
  lastName: {
    type: String,
    required: [true, 'Last name cannot be empty'],
  },
  email: {
    type: String,
    required: [true, 'Email cannot be empty'],
  },
  password: {
    type: String,
    require: [true, 'Password cannot be empty'],
  },
  mobileNo: {
    type: String,
    required: [true, 'Mobile number cannot be empty'],
  },
});

module.exports = mongoose.model('Admin', adminSchema);
