const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Product name cannot be empty'],
  },
  // price: {
  //   type: Number,
  //   required: [true, 'Price cannot be empty.'],
  // },
  description: {
    type: String,
    required: [true, 'Description cannot be empty.'],
  },
  image: {
    type: String,
    required: [true, 'Image cannot be empty'],
  },
  category: {
    type: String,
    // enum: {
    //   values: ['Coming Soon', 'Hire Item', '360 Booth'],
    //   message: '{VALUE} is not supported',
    // },
  },
});

module.exports = mongoose.model('Product', productSchema);
